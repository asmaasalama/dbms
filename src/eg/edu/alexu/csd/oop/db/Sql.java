/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.alexu.csd.oop.db;

import java.awt.TextArea;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;

/**
 *
 * @author TOSHIBA PC
 */
public class Sql implements Database {

    Action action;
    
    @Override
    public boolean executeStructureQuery(String query) throws SQLException {
        //throw new UnsupportedOperationException("Not supported yet.");
        //To change body of generated methods, choose Tools | Templates.
        action = new Action();
        boolean checkOfCreate;
        String editedQuery;
        editedQuery = Sql.regexReplace(query);
        StringTokenizer words = new StringTokenizer(editedQuery, ",");

        ArrayList<String> ColumnNames = new ArrayList<String>();
        ArrayList<String> ColumnTypes = new ArrayList<String>();

        String FirstToken = null;

        FirstToken = words.nextToken();

        String SecondToken = words.nextToken();

        String method = null;
        String TableName = null;

        if (FirstToken.equalsIgnoreCase("create") && SecondToken.equalsIgnoreCase("table") && words.hasMoreTokens()) {

            String temp = null;
            method = FirstToken;

            TableName = words.nextToken();
            if (words.hasMoreTokens()) {
                temp = words.nextToken();
                if (!temp.equalsIgnoreCase("(")) {
                    JOptionPane.showMessageDialog(null, 
                            "Please, enter -> table_name, space then (", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                    return false;

                }

            }

            if (words.hasMoreTokens()) {
                temp = words.nextToken();
            }

            while (words.hasMoreTokens() && (!(temp.equalsIgnoreCase(")")) && !(temp.equalsIgnoreCase(");")))) {
                //System.out.println(temp);
                ColumnNames.add(temp);
                if (!words.hasMoreTokens()) {
                    JOptionPane.showMessageDialog(null, 
                            "Invalid expression.", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
                temp = words.nextToken();
                //System.out.println(temp);
                if (!(temp.equalsIgnoreCase("varchar")) && !(temp.equalsIgnoreCase("int"))) {
                    JOptionPane.showMessageDialog(null, 
                            "Data type isn't supported and use right spaces in your SQL", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }

                ColumnTypes.add(temp);

                if (!words.hasMoreTokens()) {
                    JOptionPane.showMessageDialog(null, 
                            "Invalid expression.", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
                temp = words.nextToken();
            }

            String temp2 = null;
            if (words.hasMoreTokens()) {

                temp2 = words.nextToken();

                if ((!temp.equalsIgnoreCase(")") && !temp2.equalsIgnoreCase(";"))) {
                    //(ColumnNames, ColumnTypes, TableName, method);
                    //
                    try {
                        checkOfCreate = action.createTable(TableName, ColumnNames, ColumnTypes);
                        if (checkOfCreate == true) {
                            JOptionPane.showMessageDialog(null, 
                            "Table is created", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                            return true;
                        } else {
                            return false;
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SAXException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (TransformerException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //
                    
                    } else {
                    JOptionPane.showMessageDialog(null, 
                            "Invalid expression. Please enter );", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }

            } else if (!temp.equalsIgnoreCase(");")) {
                JOptionPane.showMessageDialog(null, 
                            "Invalid expression.", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                return false;
            }
            
            //CheckCreate(ColumnNames, ColumnTypes, TableName, method);
            //
            try {
                checkOfCreate = action.createTable(TableName, ColumnNames, ColumnTypes);
                if (checkOfCreate == true) {
                    JOptionPane.showMessageDialog(null, 
                            "Table is created", "Display Message ", JOptionPane.INFORMATION_MESSAGE);
                    return true;
                } else {
                    return false;
                }
            } catch (IOException ex) {
                Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
            }
            //
            return true;

        } else if (FirstToken.equalsIgnoreCase("drop") && SecondToken.equalsIgnoreCase("table") && words.hasMoreTokens()) {
            String temp = null;
            method = FirstToken;
            
            boolean checkDropTable;
            
            TableName = words.nextToken();

            if (words.hasMoreTokens() && words.nextToken().equalsIgnoreCase(";")) {
                //CheckDrop(TableName, method);
                checkDropTable = action.dropTable(TableName);
                
                if (checkDropTable == true){
                    JOptionPane.showMessageDialog(null, 
                            "Table is deleted", "Display Message ", JOptionPane.INFORMATION_MESSAGE);
                    return true;
                }
                else{
                    return false;
                }
            }

            char[] tableName = TableName.toCharArray();
            if (tableName[tableName.length - 1] != ';') {

                JOptionPane.showMessageDialog(null, 
                            "Invalid expression. Enter ';' at the end of the SQL", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                //CheckDrop(TableName, method);
                return false;

            } else {

                TableName = TableName.replace(TableName.substring(TableName.length() - 1), "");
                //CheckDrop(TableName, method);
                checkDropTable = action.dropTable(TableName);
                
                if (checkDropTable == true){
                    JOptionPane.showMessageDialog(null, 
                            "Table is deleted", "Display Message ", JOptionPane.INFORMATION_MESSAGE);
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Object[][] executeRetrievalQuery(String query) throws SQLException {
        //throw new UnsupportedOperationException("Not supported yet.");
        //To change body of generated methods, choose Tools | Templates.
        Object[][] object = new Object[100][100];
        action = new Action();
        
        String editedQuery;
        editedQuery = Sql.regexReplace(query);
        StringTokenizer words = new StringTokenizer(editedQuery, ",");

        ArrayList<String> ColumnNames = new ArrayList<String>();
        ArrayList<String> ColumnTypes = new ArrayList<String>();

        String FirstToken = null;
        String temp2 = null;

        FirstToken = words.nextToken();

        String SecondToken = words.nextToken();

        String method = null;
        String TableName = null;
        String conditionSelect = null;

        if (FirstToken.equalsIgnoreCase("select") && words.hasMoreElements()) {
            
            method = FirstToken;
            String temp = SecondToken;

            while (words.hasMoreTokens() && !temp.equalsIgnoreCase("From")) {
                ColumnNames.add(temp);
                temp = words.nextToken();

            }

            if (!words.hasMoreTokens()) {
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete", "Display Message ", JOptionPane.INFORMATION_MESSAGE);
                return null;
            } else {
                temp = words.nextToken();
                TableName = temp;
            }
            String totalCondition = null;
            if (!words.hasMoreTokens()) {
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete", "Display Message ", JOptionPane.INFORMATION_MESSAGE);
                return null;
            } else if (!words.nextToken().equalsIgnoreCase("where")) {
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete and check \"WHERE\"", "Display Message ", JOptionPane.INFORMATION_MESSAGE);
                return null;
            } else if (words.hasMoreTokens()) {
                //conditionSelect
                
                int counterOfSpaces = 0;
                totalCondition ="";
                //if (words.hasMoreTokens()) {
                    totalCondition += words.nextToken();
                        //totalCondition += totalCondition;
                        while (words.hasMoreTokens()) {
                            counterOfSpaces++;
                            totalCondition += words.nextToken();
                            //System.out.println(counterOfSpaces);
                            //System.out.println(totalCondition);
                            //String tempOfCondition = words.nextToken();
                        }
                        
                  //  }
                } else {
                    JOptionPane.showMessageDialog(null,
                        "Type the SELECT condition", "Error Message", JOptionPane.ERROR_MESSAGE);
                    return null;
                }
                
                
                
                if (totalCondition.charAt(totalCondition.length() - 1) != ';') {

                    JOptionPane.showMessageDialog(null,
                        "Put ';' at the end of the condition", "Error Message", JOptionPane.ERROR_MESSAGE);
                    return null;

                } else {
                    
                    //condition = condition.replace(condition.substring(condition.length() - 1), "");
                    //CheckDelete(TableName, method, condition);
                    String s = totalCondition;
                    StringBuilder sb = new StringBuilder(s);
                    sb.deleteCharAt(s.length()-1);
                    totalCondition = sb.toString();
                    //System.out.println(totalCondition);
                    
                    try {
                    //    if (action.selectFromTable(TableName, totalCondition, ColumnNames) == null) {
                    //        return null;
                    //    } else {
                            //System.out.println(totalCondition);
                            object = action.selectFromTable(TableName, totalCondition, ColumnNames);
                            
                            
                            
                            //if (counterOfColumns == 0){
                            JOptionPane.showMessageDialog(null,
                                    method + " statement is done", "Display message", JOptionPane.INFORMATION_MESSAGE);
//                            return object;
                            //if (counterOfColumns == 0){
                              //  return 1;
                            //}
                           
                            //return counterOfColumns;
                    //    }
                    } catch (SAXException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (TransformerException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (XPathExpressionException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        
                        
                        /*
                        if (words.hasMoreTokens()) {
                        temp2 = words.nextToken();
                        if (!temp2.equalsIgnoreCase(";")) {
                        System.out.println("Not Valid3");
                        
                        }
                        
                        } else {
                        
                        char[] c = conditionSelect.toCharArray();
                        
                        if (c[c.length - 1] != ';') {
                        
                        System.out.println("Not valid4");
                        
                        } else {
                        
                        conditionSelect = conditionSelect.replace(conditionSelect.substring(conditionSelect.length() - 1), "");
                        CheckSelect(ColumnNames, TableName, method, conditionSelect);
                        }
                        
                        }
                    */                  
                        //Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    
//            } else {
//                System.out.println("not valid4");
//            }
                }
        }
        //CheckSelect(ColumnNames, TableName, method, conditionSelect);
        //ArrayList <String> objectList = new ArrayList<String>();
        
        /*
        try {
            object = action.selectFromTable(TableName, conditionSelect, ColumnNames);
            
            //for (int i = 0; i < objectList.size(); i++){
              //  object[0][i] = objectList.get(i);
            //}
            // object[][]=[CloumNames][CloumTypes];
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        }
        //SQLException x = new SQLException("");
        //throw x;
        // return object[CloumNames.toArray()][CloumTypes.toArray()];
        */
        return object;

    }

    @Override
    public int executeUpdateQuery(String query) throws SQLException {
        // throw new UnsupportedOperationException("Not supported yet."); 
        //To change body of generated methods, choose Tools | Templates.
        
        action = new Action();

        int counterOfColumns = 0;
        
        String editedQuery;
        editedQuery = Sql.regexReplace(query);
        StringTokenizer words = new StringTokenizer(editedQuery, ",");

        ArrayList<String> ColumnNames = new ArrayList<String>();
        ArrayList<String> ColumnTypes = new ArrayList<String>();
        ArrayList<String> ColumnValues = new ArrayList<String>();

        String FirstToken = null;

        FirstToken = words.nextToken();

        String SecondToken = words.nextToken();

        String method = null;
        String TableName = null;

        if (FirstToken.equalsIgnoreCase("insert") && SecondToken.equalsIgnoreCase("into") && words.hasMoreTokens()) {

            method = FirstToken;
            TableName = words.nextToken();
            String temp = null;

            String resultStringOfColumn = null;
            String resultStringOfColumnContinued = null;
            int flag = 0;
            if (words.hasMoreTokens()) {
                temp = words.nextToken();
                
                //System.out.println(resultString);
                
                if (temp.equalsIgnoreCase("(")) {
                    if (words.hasMoreTokens()){
                    JOptionPane.showMessageDialog(null, 
                            "Don't enter space after '('", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    //System.out.println("not valid1");
                    return 0;
                    }
                }
                //System.out.println(temp);
                if (temp.charAt(0) == '(' && temp.charAt(temp.length() - 1) == ')') {
                    StringBuilder sb = new StringBuilder(temp);
                    sb.deleteCharAt(0);

                    resultStringOfColumn = sb.toString();
                    //System.out.println(resultStringOfColumn);
                    //System.out.println(resultStringOfColumn);
                    StringBuilder sb2 = new StringBuilder(resultStringOfColumn);

                    sb2.deleteCharAt(resultStringOfColumn.length() - 1);
                    String s = sb2.toString();
                    //resultStringOfColumn = sb.toString();
                    ColumnNames.add(s);
                    //System.out.println(s);
                    flag = 1;
                }
                else if (temp.charAt(0) == '(' && temp.charAt(temp.length()-1) != ')'){
                    
                    StringBuilder sb2 = new StringBuilder(temp);
                    sb2.deleteCharAt(0);
                    resultStringOfColumn = sb2.toString();
                    ColumnNames.add(resultStringOfColumn);  
                }
                else if (temp.charAt(0) != '(' && temp.charAt(temp.length()-1) != ')'){
                    JOptionPane.showMessageDialog(null, 
                            "Please, enter '(', Names of columns, ')' ", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    return 0;
                }
                /*
                else{
                    JOptionPane.showMessageDialog(null, 
                            "Please, enter '(' before the first column name", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    return 0;
                }*/
            } 
            else {
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't right", "Error Message ", JOptionPane.ERROR_MESSAGE);
                //System.out.println("not valid2");
                return 0;
            }

            if (words.hasMoreTokens()) {
                temp = words.nextToken();
                //if (!temp.equals(",")){
                  //  JOptionPane.showMessageDialog(null, 
                    //        "Enter space after ','", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                //}
            } else {
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete", "Error Message ", JOptionPane.ERROR_MESSAGE);
                //System.out.println("not valid3");
                return 0;
            }
            while (flag == 0 && words.hasMoreTokens() && temp.charAt(temp.length()-1) != ')') {
                
                ColumnNames.add(temp);
                //counterOfColumns++;
                temp = words.nextToken();

            }
            //System.out.println(temp);
            if (!words.hasMoreTokens() && flag == 0){
                JOptionPane.showMessageDialog(null,
                            method+" statement isn't complete", "Error Message ", JOptionPane.ERROR_MESSAGE);
                return 0;
            }
            else if (flag == 0){
                if (temp.equals(")")) {
                    JOptionPane.showMessageDialog(null,
                            "Don't enter space before ')'.", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    return 0;
                } else if (flag == 0 && temp.charAt(temp.length() - 1) == ')') {
                    StringBuilder sb3 = new StringBuilder(temp);
                    sb3.deleteCharAt(temp.length() - 1);
                    resultStringOfColumn = sb3.toString();
                    ColumnNames.add(resultStringOfColumn);
                } else if (flag == 0) {
                    JOptionPane.showMessageDialog(null,
                            " Enter ')' after the last column name", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    return 0;
                }
            
            }
           
            
            if (flag == 0 && !words.hasMoreTokens()) {
                //if (temp.charAt(temp.length()-2) != ')' || temp.charAt(temp.length()-1) != ';'){
                    //JOptionPane.showMessageDialog(null, 
                      //      "\"Please, enter ');' after the last column name.", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    //return 0;
                //}
                //else{
                //System.out.println("l;k;l");
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete", "Error Message ", JOptionPane.ERROR_MESSAGE);
                return 0;
                //System.out.println("not valid4");
                //return 0;
                //}                
            }
            else if (flag == 0){
                temp = words.nextToken();
                //System.out.println(temp);
            }
            //if (!temp.equalsIgnoreCase(")")) {
            //    String s = temp;
                
                
                //System.out.println("not valid5");
            //}
            
            if (!temp.equalsIgnoreCase("values")) {
                
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't valid. Take a look at the word \"VALUES\"", "Error Message ", JOptionPane.ERROR_MESSAGE);
                //System.out.println("not valid6");
                return 0;
            }
            if (!words.hasMoreTokens()){
                //System.out.println("adssad");
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete", "Error Message ", JOptionPane.ERROR_MESSAGE);
                return 0;
            }
            else{
                temp = words.nextToken();
                //System.out.println(temp);
                if (flag == 1) {
                    
                    if (temp.equals("(")) {
                        JOptionPane.showMessageDialog(null,
                                "Don't enter space after '(' ", "Error Message ", JOptionPane.ERROR_MESSAGE);
                        return 0;
                    }
                    int flagOfOneCol = 0;
                    if (temp.charAt(temp.length() - 2) == ')' && temp.charAt(temp.length() - 1) == ';') {
                        StringBuilder sb = new StringBuilder(temp);
                        sb.deleteCharAt(temp.length() - 2);
                        resultStringOfColumnContinued = sb.toString();
                        flagOfOneCol = 1;
                        String string;
                        StringBuilder sb2 = new StringBuilder(resultStringOfColumnContinued);
                        sb2.deleteCharAt(resultStringOfColumnContinued.length() - 1);
                        string = sb2.toString();
                        //ColumnValues.add(string);
                        //System.out.println(string);
                        
                        StringBuilder sb3 = new StringBuilder(string);
                        sb2.deleteCharAt(0);
                        String lastString = sb2.toString();
                        ColumnValues.add(lastString);
                        //System.out.println(lastString);
                        //System.out.println(ColumnNames.size());
                    }
                    if (words.hasMoreTokens() || flagOfOneCol == 0) {
                        JOptionPane.showMessageDialog(null,
                                "Don't enter spaces between brackets and use ';'", "Error Message ", JOptionPane.ERROR_MESSAGE);
                        return 0;
                    }
                   
                } else if (flag == 0) {
                    String firstValue, lastValue;
//                    System.out.println(";fkdsfk;ldfsk");
//                    if (words.hasMoreTokens()) {
//                        JOptionPane.showMessageDialog(null,
//                                    method+" statement is wrong", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
//                    }
                        
                        //System.out.println(temp);
                        if (temp.equalsIgnoreCase("(")) {
                            JOptionPane.showMessageDialog(null,
                                    "Don't enter space after '('", "Error Message ", JOptionPane.ERROR_MESSAGE);
                            return 0;
                        } else if (temp.charAt(0) == '(') {
                            //System.out.println(temp);
                            StringBuilder sb = new StringBuilder(temp);
                            sb.deleteCharAt(0);
                            firstValue = sb.toString();
                            ColumnValues.add(firstValue);
                            //System.out.println(firstValue);
                        } else if (temp.charAt(0) != '(') {
                            //System.out.println(temp);
                            JOptionPane.showMessageDialog(null,
                                    "Please, enter '(' before the first value", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                            return 0;
                        }
                    
                        else {
                            JOptionPane.showMessageDialog(null,
                                    method + " statement is wrong", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                            return 0;
                        }
                    if (words.hasMoreTokens() && temp.charAt(temp.length() - 2) != ')'
                            && temp.charAt(temp.length() - 1) != ';') {
                        temp = words.nextToken();
//                        if (temp.charAt(0) == ';' || temp.charAt(0) == ')' || temp.equals(");")){
//                            JOptionPane.showMessageDialog(null,
//                                    "Don't use spaces in ');'", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
//                            return 0;
//                        }
                        
                            while (temp.length() >=2 
                                    &&temp.charAt(temp.length() - 2) != ')'
                                    && temp.charAt(temp.length() - 1) != ';') {
                                
                                ColumnValues.add(temp);
                                if (words.hasMoreTokens()) {
                                    temp = words.nextToken();
                                    //System.out.println(temp);
                                } else {
                                    JOptionPane.showMessageDialog(null,
                                            "Check the end of the names of columns", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                                    return 0;
                                }
                            }
                        
                        //System.out.println(temp);
                        if (temp.charAt(temp.length()-1) != ';'){
                            //System.out.println("asmaasalama");
                            JOptionPane.showMessageDialog(null,
                                "Invalid "+method+" statement", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                            return 0;
                        }
                    } else if (words.hasMoreTokens() && (temp.charAt(temp.length() - 2) != ')'
                            || temp.charAt(temp.length() - 1) != ';')) {
                        JOptionPane.showMessageDialog(null,
                                "Enter ');' at the end", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                        return 0;
                    }
                    //System.out.println(temp);
                    String lastOrder = null;
                    //System.out.println(temp);
                    if ( temp.length() > 2 && temp.charAt(temp.length() - 2) == ')' && temp.charAt(temp.length() - 1) == ';') {
                        lastOrder = ");";
                        StringBuilder sb = new StringBuilder(temp);
                        sb.deleteCharAt(temp.length() - 2);
                        lastValue = sb.toString();
                        sb.deleteCharAt(lastValue.length() - 1);
                        lastValue = sb.toString();
                        
//                        StringBuilder sb2 = new StringBuilder(lastValue);
//                        sb2.deleteCharAt(0);
//                        lastValue = sb2.toString();
                        
                        ColumnValues.add(lastValue);
                        //System.out.println(lastValue);
                        //System.out.println(ColumnValues.size());
                    } else if ( temp.length() == 2 
                            && (temp.charAt(temp.length() - 2) != ')' || temp.charAt(temp.length() - 1) != ';')) {
                        JOptionPane.showMessageDialog(null,
                                "Don't enter space before ');'", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                        return 0;
                    }
                    else if (temp.length() == 2 
                            && (temp.charAt(temp.length() - 2) == ')' || temp.charAt(temp.length() - 1) == ';')){
                        JOptionPane.showMessageDialog(null,
                                "Don't enter space before ');'", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                        return 0;
                    }
                    else if (temp.length() == 1){
                        JOptionPane.showMessageDialog(null,
                                "Invalid "+method+" statement", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                        return 0;
                    }
                }    
            }
            
            
            /*
            if (words.hasMoreTokens()) {
                temp = words.nextToken();
            } else {
                System.out.println("not valid9");
                return 0;
            }
            */
            /*if (flag == 1){
                String s = temp;
                StringBuilder sb3 = new StringBuilder(s);
                System.out.println(temp);
                
                sb3.deleteCharAt(s.charAt(0));
                System.out.println(s);
                sb3.deleteCharAt(s.length()-1);
                sb3.deleteCharAt(s.length()-2);
                firstValue = sb3.toString();
                System.out.println(firstValue);
                ColumnValues.add(firstValue);
            }*/
            

            //String temp2 = null;
            if (!words.hasMoreTokens()) {
                //temp2 = words.nextToken();

               //if (lastOrder.equals(");")) {
                    //CheckCreate(ColumnNames, ColumnValues, TableName, method);
                    try {
                        counterOfColumns = action.insertIntoTable(TableName, ColumnNames, ColumnTypes, ColumnValues);
                        //System.out.println(";dlkfd;lfk");
                    } catch (IOException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SAXException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (TransformerException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (XPathExpressionException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //return counterOfColumns;
                    return counterOfColumns;
                //} else {
                  //  JOptionPane.showMessageDialog(null, 
                    //        "SQL statement isn't valid. Check this ');'", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                    //return 0;
                
            } else {
                JOptionPane.showMessageDialog(null,
                                "Invalid INSERT statement", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                //System.out.println("not valid11");
                return 0;
            }
       
        } else if (FirstToken.equalsIgnoreCase("delete") && SecondToken.equalsIgnoreCase("from") && words.hasMoreTokens()) {
            method = FirstToken;
            String thirdToken = null;
            TableName = words.nextToken();
            int counterOfSpaces = 0;
            if (words.hasMoreTokens()) {
                thirdToken = words.nextToken();
            } else {
                JOptionPane.showMessageDialog(null, 
                            method+" statement isn't complete.", "Error Message", JOptionPane.ERROR_MESSAGE);
                return 0;
            }
            if (!thirdToken.equalsIgnoreCase("where")) {
                JOptionPane.showMessageDialog(null, 
                            "WHERE isn't written well", "Error Message", JOptionPane.ERROR_MESSAGE);
                return 0;
            }

            String condition = null;

            
//                if (words.hasMoreTokens() && words.nextToken().equalsIgnoreCase(";")) {
//                    CheckDelete(TableName, method, condition);
//                    System.out.println("jaljfak");
//                    try {
//                        counterOfColumns = action.deleteFromTable(TableName, condition);
//                    } catch (ParserConfigurationException ex) {
//                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (SAXException ex) {
//                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (IOException ex) {
//                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (XPathExpressionException ex) {
//                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (JDOMException ex) {
//                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (TransformerException ex) {
//                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                    return counterOfColumns;

                //}
                String totalCondition ="";
                if (words.hasMoreTokens()) {
                    condition = words.nextToken();
                        totalCondition += condition;
                        while (words.hasMoreTokens()) {
                            totalCondition += words.nextToken();
                            counterOfSpaces++;
                            //System.out.println(counterOfSpaces);
                            //System.out.println(totalCondition);
                           // String tempOfCondition = words.nextToken();
                        }
                    //}
                } else {
                    JOptionPane.showMessageDialog(null,
                        "Type the condition", "Error Message", JOptionPane.ERROR_MESSAGE);
                    return 0;
                }
                //if (counterOfSpaces > 0){
                    
                    //String s = regexReplace(condition);
                  //  System.out.println(totalCondition);
                //}
//                if (words.hasMoreTokens()){
//                    JOptionPane.showMessageDialog(null,
//                        "Don't write space after the condition", "Error Message", JOptionPane.ERROR_MESSAGE);
//                    return 0;
//                }
                //System.out.println(condition);
                if (totalCondition.charAt(totalCondition.length() - 1) != ';') {

                    JOptionPane.showMessageDialog(null,
                        "Put ';' at the end of the condition", "Error Message", JOptionPane.ERROR_MESSAGE);
                    return 0;

                } else {
                    
                    //condition = condition.replace(condition.substring(condition.length() - 1), "");
                    //CheckDelete(TableName, method, condition);
                    String s = totalCondition;
                    StringBuilder sb = new StringBuilder(s);
                    sb.deleteCharAt(s.length()-1);
                    totalCondition = sb.toString();
                    try {
                        //if (action.deleteFromTable(TableName, totalCondition) == -1) {
                         //   return 0;
                        //} else {
                            //System.out.println(totalCondition);
                            counterOfColumns = action.deleteFromTable(TableName, totalCondition);
                            
                            if (counterOfColumns == -1){
                                return 0;
                            }
                            
                            //if (counterOfColumns == 0){
                            JOptionPane.showMessageDialog(null,
                                    method + " statement is done", "Display message", JOptionPane.INFORMATION_MESSAGE);
                            if (counterOfColumns == 0){
                                return 1;
                            }
                           
                            return counterOfColumns;
                        //}
                        //}
                        //else if (action.deleteFromTable(TableName, totalCondition) == 0){
                            
                        //}
              
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SAXException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (XPathExpressionException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (JDOMException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (TransformerException ex) {
                        Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            JOptionPane.showMessageDialog(null,
                    "Successful delete process", "Display message", JOptionPane.INFORMATION_MESSAGE);
        }
        
        return counterOfColumns;
    }

    public static void regexChecker(String theRegex, String str2Check) {
        Pattern checkRegex = Pattern.compile(theRegex);
        Matcher regexMatcher = checkRegex.matcher(str2Check);

        while (regexMatcher.find()) {
            if (regexMatcher.group().length() != 0) {
                System.out.println(regexMatcher.group().trim());
            }
            //   System.out.println("Start index: "+regexMatcher.start());
            // System.out.println("End of index: "+regexMatcher.end());

        }
    }

    public static String regexReplace(String str2Replace) {
        // Pattern replace =Pattern.compile("[A-Za-z]",Pattern.CASE_INSENSITIVE);
        Pattern replace = Pattern.compile("\\s+");
        Matcher regexMatcher = replace.matcher(str2Replace.trim());
        //System.out.println(regexMatcher.replaceAll(","));
        return regexMatcher.replaceAll(",");
    }

    public int validate(String code) {
        String method;
        String editedCode;
        int flag = 1;

        editedCode = regexReplace(code);
        System.out.println(editedCode);
        StringTokenizer words = new StringTokenizer(editedCode, ",");
        ArrayList<String> ColumnNames = new ArrayList<String>();
        ArrayList<String> ColumnTypes = new ArrayList<String>();

        String FirstToken = null;

        FirstToken = words.nextToken();

        String SecondToken = null;
        if (words.hasMoreTokens() && flag == 1) {
            SecondToken = words.nextToken();

        } else {
            return 0;
        }
        String TableName = null;

        if (FirstToken.equalsIgnoreCase("create") && SecondToken.equalsIgnoreCase("table") && words.hasMoreTokens() && flag == 1) {

            String temp = null;
            method = FirstToken;

            TableName = words.nextToken();
            if (words.hasMoreTokens()) {
                temp = words.nextToken();
                if (!temp.equalsIgnoreCase("(")) {

                    return 0;

                }

            }

            if (words.hasMoreTokens()) {
                temp = words.nextToken();
            }

            while (words.hasMoreTokens() && !temp.equalsIgnoreCase(")")) {
                //System.out.println(temp);
                ColumnNames.add(temp);
                if (!words.hasMoreTokens()) {
                    return 0;
                }
                temp = words.nextToken();
                //System.out.println(temp);
                if (!((temp.equalsIgnoreCase("varchar"))) && !(temp.equalsIgnoreCase("int"))) {
                    return 2;
                }
                ColumnTypes.add(temp);

                if (!words.hasMoreTokens()) {
                    return 0;
                }
                temp = words.nextToken();
            }
            if (!temp.equalsIgnoreCase(");")) {
                return 0;
            }

        } else if (FirstToken.equalsIgnoreCase("delete") && SecondToken.equalsIgnoreCase("from") && words.hasMoreTokens() && flag == 1) {
            method = FirstToken;
            String ThirdToken = null;
            TableName = words.nextToken();
            if (words.hasMoreTokens()) {
                ThirdToken = words.nextToken();
            } else {
                return 0;
            }
            if (!ThirdToken.equalsIgnoreCase("where")) {
                return 5;
            } else {

                String condition = null;
                if (words.hasMoreTokens()) {
                    condition = words.nextToken();

                    if (words.hasMoreTokens() && words.nextToken().equalsIgnoreCase(";")) {
                        CheckDelete(TableName, method, condition);
                        return 1;

                    }

                    char[] c = condition.toCharArray();
                    if (c[c.length - 1] != ';') {

                        CheckDelete(TableName, method, condition);
                        return 3;

                    } else {

                        condition = condition.replace(condition.substring(condition.length() - 1), "");
                        CheckDelete(TableName, method, condition);
                    }

                    CheckDelete(TableName, method, condition);

                } else {
                    return 4;
                }

            }

        } else if (FirstToken.equalsIgnoreCase("drop") && SecondToken.equalsIgnoreCase("table") && words.hasMoreTokens() && flag == 1) {
            String temp = null;
            method = FirstToken;

            TableName = words.nextToken();

            if (words.hasMoreTokens() && words.nextToken().equalsIgnoreCase(";")) {
                CheckDrop(TableName, method);
                return 1;

            }

            char[] tableName = TableName.toCharArray();
            if (tableName[tableName.length - 1] != ';') {

                CheckDrop(TableName, method);
                return 3;

            } else {

                TableName = TableName.replace(TableName.substring(TableName.length() - 1), "");
                CheckDrop(TableName, method);
            }
        } else if (FirstToken.equalsIgnoreCase("insert") && SecondToken.equalsIgnoreCase("into") && words.hasMoreTokens() && flag == 1) {

            method = FirstToken;
            TableName = words.nextToken();
            words.nextToken();
            String s1 = words.nextToken();
            while (words.hasMoreTokens() && s1 != ")") {

                ColumnNames.add(s1);
                s1 = words.nextToken();

            }
            words.nextToken();
            if (!words.nextToken().equalsIgnoreCase("value")) {
                return 0;
            } else {
                words.nextToken();

            }

        } else if (FirstToken.equalsIgnoreCase("select") && words.hasMoreElements() && flag == 1) {

            method = FirstToken;
            String temp = SecondToken;
            String conditionSelect = null;

            while (words.hasMoreTokens() && !temp.equalsIgnoreCase("From")) {
                ColumnNames.add(temp);
                temp = words.nextToken();

            }

            if (!words.hasMoreTokens()) {
                return 0;
            } else {
                TableName = words.nextToken();
            }

            if (!words.hasMoreTokens()) {
                return 5;
            } else if (!words.hasMoreTokens()) {
                return 4;
            } else {
                conditionSelect = words.nextToken();
            }
            //CheckSelect(ColumnNames, TableName, method, conditionSelect);

        } else {
            return 0;
        }

        // TableName=words.nextToken();
        // words.nextToken();
        //String s1=words.nextToken();
        /*  
while 
    (words.hasMoreTokens()&&s1!=")")
    {

        
    CloumNames.add(s1);
     CloumTypes.add(words.nextToken());
      s1=words.nextToken();
    }
         */
        return flag;

    }

    public void CheckCreate(ArrayList ColumnNames, ArrayList ColumnTypes, String TableName, String Method) {

        System.out.println("Table name :" + TableName);
        System.out.println("Method name :" + Method);

        System.out.println("Column Names :");

        for (int i = 0; i < ColumnNames.size(); i++) {
            System.out.println(ColumnNames.get(i));
        }

        System.out.println("Column Types :");

        for (int i = 0; i < ColumnTypes.size(); i++) {
            System.out.println(ColumnTypes.get(i));
        }

    }

    public void CheckDelete(String TableName, String Method, String condition) {
        System.out.println("Table name :" + TableName);
        System.out.println("Method name :" + Method);
        System.out.println("Condition :" + condition);

    }

    public void CheckDrop(String TableName, String Method) {
        System.out.println("Table name :" + TableName);
        System.out.println("Method name :" + Method);

    }

    public void CheckSelect(ArrayList ColumnNames, String TableName, String Method, String condition) {

        System.out.println("Table name :" + TableName);
        System.out.println("Method name :" + Method);

        System.out.println("Column Names :");

        for (int i = 0; i < ColumnNames.size(); i++) {
            System.out.println(ColumnNames.get(i));

        }
        System.out.println("Condition :" + condition);

    }

}
