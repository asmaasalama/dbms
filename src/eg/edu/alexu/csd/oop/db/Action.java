/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.alexu.csd.oop.db;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

// Used to write data to a file
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

// Triggered when an I/O error occurs
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.script.ScriptEngine.FILENAME;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
/*
import static java.util.stream.DoubleStream.builder;
import static java.util.stream.IntStream.builder;
import static java.util.stream.LongStream.builder;
import static javax.script.ScriptEngine.FILENAME;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
 */
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//
//import javax.xml.parsers.SAXParserFactory;
//import javax.xml.soap.Node;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerConfigurationException;
//import javax.xml.transform.TransformerException;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamResult;
//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathConstants;
//import javax.xml.xpath.XPathExpressionException;
//import javax.xml.xpath.XPathFactory;
import org.jdom2.Content;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

// Represents your XML document and contains useful methods
import org.jdom2.Document;

// Represents XML elements and contains useful methods
import org.jdom2.Element;
import org.jdom2.JDOMException;

// Represents text used with JDOM
import org.jdom2.Text;
import org.jdom2.filter.Filter;
import static org.jdom2.filter.Filters.element;
import org.jdom2.input.DOMBuilder;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.DOMOutputter;

// Formats how the XML document will look
import org.jdom2.output.Format;

// Outputs the JDOM document to a file
import org.w3c.dom.*;
import org.jdom2.output.XMLOutputter;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import static org.jdom2.filter.Filters.element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

/**
 *
 * @author asmaa
 */
public class Action {

    public boolean createTable(String tableName, List<String> columnsNames, List<String> columnsDataTypes) throws IOException, ParserConfigurationException, SAXException, TransformerConfigurationException, TransformerException {
        {
            Document doc, docDTD;
            BufferedWriter bw = null;
            String fileName = tableName + ".xml";

            File idea = new File("xml/" + fileName);
            

            if (!idea.exists()) {
                FileWriter fw = new FileWriter(idea);
                idea.getParentFile().mkdirs();
                try {
                    //fw = new FileWriter("xml/"+fileName);

                    try {

                        bw = new BufferedWriter(fw);
                        doc = new Document();
                        //bw.write(content);
                        //System.out.println("Done");
                        //bw.write("<!DOCTYPE "+tableName+" SYSTEM \""+tableName+".dtd\">");

                        //String xmlString = "<!DOCTYPE "+tableName+" SYSTEM \""+tableName+".dtd\">";
                        //doc.setContent((Content)xmlString);
                        // Creates a JDOM document
                        // Creates an element named table and makes it the root
                        Element theRoot = new Element(tableName);
                        doc.setRootElement(theRoot);

                        Element show = new Element("row");

                        Element col;

                        int i = 0;

                        for (String c : columnsNames) {
                            col = new Element(c);
                            //name = new Element(columnsDataTypes.get(i));

                            // Just a default text in the elements in the whole node
                            col.addContent(new Text("\u200B"));

                            show.addContent(col);
                            // Adds the show tag and all of its children to the root
                            //theRoot.addContent(col);
                            i++;
                        }
                        theRoot.addContent(show);
                        /*
                        Element show2 = new Element("row");

                        Element col2;

                        int j = 0;

                        for (String c : columnsNames) {
                            col2 = new Element(c);
                            //name = new Element(columnsDataTypes.get(i));

                            // Just a default text in the elements in the whole node
                            //col2.addContent(new Text(columnsDataTypes.get(j)));

                            show2.addContent(col2);
                            // Adds the show tag and all of its children to the root
                            //theRoot.addContent(col);
                            j++;
                        }
                        theRoot.addContent(show2);
                         */
 /*

                        // Add a new show element like above
                        Element show = new Element("node2");

                        Element name = new Element("name");

                        name.setAttribute("show_id", "show_002");

                        name.addContent(new Text("Freaks and Geeks"));

                        Element network = new Element("network");

                        network.addContent(new Text("ABC"));

                        network.setAttribute("country", "US");

                        show.addContent(name);
                        show.addContent(network);

                        theRoot.addContent(show);
                         */
                        // Uses indenting with pretty format
                        XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());

                        // Create a new file and write XML to it
                        //xmlOutput.output(doc, new FileOutputStream(fileName));
                        xmlOutput.output(doc, fw);
                        //System.out.println("Wrote to file");

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    if (bw != null) {
                        bw.close();
                    }
                    if (fw != null) {
                        fw.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "File exists already", "Error Message ", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        BufferedWriter bw2 = null;
        FileWriter fw2 = null;
        Document docDTD;
        String dtdFileName = tableName + ".dtd";
        try {

            fw2 = new FileWriter("xml/" + dtdFileName);
            bw2 = new BufferedWriter(fw2);

            docDTD = new Document();
            XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
            xmlOutput.output(docDTD, fw2);

            bw2.write("<!DOCTYPE " + tableName + " [\n");

            bw2.write("<!ELEMENT " + tableName + " (row)>");

            bw2.write("\n<!ELEMENT row (");

            for (String c : columnsNames) {
                bw2.write(c);
                if (!c.equals(columnsNames.get(columnsNames.size() - 1))) {
                    bw2.write(",");
                }
            }
            bw2.write(")>\n");

            for (String c : columnsDataTypes) {
                bw2.write("<!ELEMENT " + c);
                if (c.equalsIgnoreCase("int")) {
                    bw2.write("(#number)>\n");
                } else if (c.equalsIgnoreCase("varchar")) {
                    bw2.write("(#PCDATA)>\n");
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Data type isn't supported.", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            bw2.write("]>");

            if (bw2 != null) {
                bw2.close();
            }
            if (fw2 != null) {
                fw2.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        /*
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setValidating(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        builder.setErrorHandler(new ErrorHandler() {
            public void error(SAXParseException exception) throws SAXException {
                // do something more useful in each of these handlers
                exception.printStackTrace();
            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {
                exception.printStackTrace();
            }

            @Override
            public void warning(SAXParseException exception) throws SAXException {
                exception.printStackTrace();
            }
        });

        org.w3c.dom.Document xmlDocument = builder.parse(new FileInputStream("xml/"+tableName+".xml"));
        DOMSource source = new DOMSource(xmlDocument);
        StreamResult result = new StreamResult(System.out);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "fileinfo.dtd");
        transformer.transform(source, result);
        */
//        DOMBuilder in = new DOMBuilder();
        
//        org.w3c.dom.Document d = builder.parse("xml/"+tableName+".xml");
//        Document jdomDoc = in.build(d);
        
        return true;
    }

    public boolean dropTable(String tableName) {
        int flag = 0;
        try {

            File fileXML = new File("xml/" + tableName + ".xml");
            File fileDTD = new File("xml/" + tableName + ".dtd");

            if (fileXML.delete()) {
                //System.out.println(fileXML.getName() + " is deleted!");
                flag = 1;
            } else {
                //  System.out.println("File doesn't exist");
                JOptionPane.showMessageDialog(null, "File doesn't exist", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                return false;
            }
            if (fileDTD.delete()) {
                //System.out.println(fileDTD.getName() + " is deleted!");
                flag = 2;
            } else {
                //  System.out.println("File doesn't exist.");
                JOptionPane.showMessageDialog(null, "File doesn't exist", "Error Message ", JOptionPane.INFORMATION_MESSAGE);
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (flag == 2) {
            // JOptionPane.showMessageDialog(null, "Table ie deleted ", "Successful Message ", JOptionPane.INFORMATION_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public int insertIntoTable(String tableName, List<String> columnsNames, List<String> columnsDataTypes, List<String> columnsContents) throws IOException, ParserConfigurationException, SAXException, TransformerConfigurationException, TransformerException, XPathExpressionException {

        int counterOfColumns = 0, flag = 0;
        
        if (columnsContents.size() != columnsNames.size()) {
            JOptionPane.showMessageDialog(null,
                    "# of columns contents isn't equal to the # of columns contents", "Error Message ", JOptionPane.ERROR_MESSAGE);
            return 0;
        }
        String fileName = tableName + ".xml";
        String path = "xml/" + fileName;

        File tableFile = new File(path);
        if (!tableFile.exists()) {
            JOptionPane.showMessageDialog(null,
                    "You didn't create that table", "Error Message ", JOptionPane.ERROR_MESSAGE);
            return 0;
        } else {

            for (int dummy = 0; dummy < columnsNames.size(); dummy++) {
                counterOfColumns++;
            }
            
            
            try {

                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

                org.w3c.dom.Document doc = docBuilder.newDocument();

                // Solving the problem of -> writing the tableName besides the xml version    
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();

                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");

                // Convert from org.w3c.Document into JDOM Document
                DOMBuilder in = new DOMBuilder();
                Document jdomDoc = in.build(doc);

                doc = docBuilder.parse(path);

                Node ay7aga = doc.getFirstChild();

                Node table = doc.getElementsByTagName("row").item(0);
                //if (table.getChildNodes() != null){
                NodeList list = table.getChildNodes();
                
//                for (int as = 0; as < list.getLength(); as++){
//                    Node no = list.item(as);
//                    System.out.println(no.getNodeName());
//                }
                
//                for (int asm  = 0; asm < columnsNames.size(); asm++){
//                    System.out.println(columnsNames.get(asm));
//                }
                
                int j = 0;
                Node node;

                for (int i = 0; i < list.getLength(); i++) {
                    node = list.item(i);
                    //System.out.println(node.getNodeValue());
                    //System.out.println(node);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        if (!node.getTextContent().equals("\u200B")) {
                            //System.out.println(node.getTextContent());
                            //System.out.println(elementForNode.getText());
                            //System.out.println("asdsdasdsd");
                            flag = 1;
                            break;
                        }
                    }
                }

                // Check if the data types of the given contents applies to the data types in the file
                File dtdFile = new File("xml/" + tableName + ".dtd");

                Scanner s = new Scanner(new FileReader(dtdFile));

                ArrayList<String> columnsDataTypesFromDTD = new ArrayList<String>();
                ArrayList<String> columnsContentsDataTypes = new ArrayList<String>();

                int counterOfDataTpes = 0;

                String line;
                line = s.nextLine();
                line = s.nextLine();
                line = s.nextLine();
                line = s.nextLine();
                //System.out.println(line);
                while (s.hasNext()) {
                    line = s.nextLine();
                    if (line.equals("]>")) {
                        break;
                    }
                    String tokens[] = line.split("#");
                    //System.out.println(tokens[1]);
                    if (tokens[1].equals("PCDATA)>")) {
                        columnsDataTypesFromDTD.add("String");
                        //System.out.println(tokens[1]);
                        counterOfDataTpes++;
                    } else if (tokens[1].equals("number)>")) {
                        columnsDataTypesFromDTD.add("int");

                        //System.out.println(counterOfDataTpes);
                        counterOfDataTpes++;
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Data type isn't supported.", "Error Message ", JOptionPane.ERROR_MESSAGE);
                        return 0;
                    }
                }
                
                ArrayList<String> columnsNamesInXML = new ArrayList<String>();
                for (int count = 0; count < list.getLength(); count++){
                    Node newOne = list.item(count);
                    if (newOne.getNodeType() == Node.ELEMENT_NODE){
                        columnsNamesInXML.add(newOne.getNodeName());
                        //System.out.println(newOne.getNodeName());
                    }
                }
                int flagOfInsert = 0;
                //System.out.println(columnsNamesInXML.size());
                //System.out.println(columnsNames.size());
                
                if (columnsNamesInXML.size() != columnsNames.size()){
                    flagOfInsert = 1;
                    //System.out.println(flagOfInsert);
                }
                String colContent, dummyDataType;
                char colContentChar;
                int counterOfWord, counterOfInt = 0, counterOfColumnsNames = 0, counterOfDataTypeFromDtd = 0;
                //int counterOfDataType = 0;
                Node dummyNode;
                int counterOfcolumnsNames = 0;
                ArrayList<String> columnsDTOfDTDRistricted = new ArrayList<String>();
                int counterOfcolContent = 0;
                if (flagOfInsert == 0){
                for (int i = 0; i < list.getLength(); i++) {
                    counterOfWord = 0;
                    counterOfInt = 0;
                    if (counterOfcolContent != columnsContents.size()){
                    colContent = columnsContents.get(counterOfcolContent);
                    //dummyNode = list.item(i);
                    
                    //colContentChar = colContent.charAt(counterOfWord);
                    while (counterOfWord < colContent.length()) {
                        colContentChar = colContent.charAt(counterOfWord);
                        //System.out.println(colContentChar);
                        if (Character.isDigit(colContentChar)) {
                            counterOfInt++;
                            
                        }
                        counterOfWord++;
                    }
                       //System.out.println(counterOfInt);
                    if (counterOfInt == colContent.length()) {
                        columnsContentsDataTypes.add("int");
                        dummyDataType = "int";
                        //System.out.println("int");
                    } else {
                        columnsContentsDataTypes.add("String");
                        dummyDataType = "String";
                        //System.out.println("string");
                    }
                    counterOfcolContent++;
                    /*
                        if (dummyNode.getNodeType() == Node.ELEMENT_NODE
                                && dummyNode.getNodeName().equals(columnsNames.get(counterOfColumnsNames))) {
                            
                            //if (//dummyNode.getNodeName().equals(columnsNames.get(counterOfColumnsNames))
                              //       !columnsDataTypesFromDTD.get(counterOfDataTypeFromDtd).equals(dummyDataType)) {
                                columnsDTOfDTDRistricted.add(dummyDataType);
                                counterOfColumnsNames++;
                                counterOfcolContent++;
                                System.out.println(dummyNode.getNodeName());
                                //counterOfDataTpes++;
                            //}
                            //counterOfDataTypeFromDtd++;
                        }
                    //System.out.println(dummyNode.getNodeName()); 
                    */
                    }
                }
                for (int counterOfDTD = 0; counterOfDTD < columnsContentsDataTypes.size(); counterOfDTD++){
                    if (!columnsDataTypesFromDTD.get(counterOfDTD).equals(columnsContentsDataTypes.get(counterOfDTD))) {
                        //if (){
                            
                        //}
                        JOptionPane.showMessageDialog(null,
                                "Incompitable data types..", "Display message ", JOptionPane.INFORMATION_MESSAGE);
                        return 0;
                    }
                }
                }
                else{
                    char c;
                    int counterOfWord2, counterOfInt2;
                    ArrayList<String> columnsDataTypesNew = new ArrayList<String>();
                    String currentColumnValue;
                    counterOfcolumnsNames = 0;
                    for (int index = 0; index < columnsNamesInXML.size(); index++) {
                        //System.out.println(columnsNames.get(index));
                        //System.out.println(columnsNamesInXML.get(counterOfcolumnsNamesXML));
                        if (counterOfcolumnsNames != columnsNames.size()
                               && columnsNames.get(counterOfcolumnsNames).equals(columnsNamesInXML.get(index))) {
                            currentColumnValue = columnsContents.get(counterOfcolumnsNames);
                            counterOfInt2 = 0;
                            counterOfWord2 = 0;
                            //System.out.println(index);
                            while (counterOfWord2 < currentColumnValue.length()) {
                                c = currentColumnValue.charAt(counterOfWord2);
                                //System.out.println(colContentChar);
                                if (Character.isDigit(c)) {
                                    counterOfInt2++;

                                }
                                counterOfWord2++;
                            }
                            //System.out.println(counterOfInt2);
                            if (counterOfInt2 == currentColumnValue.length()) {
                                columnsDataTypesNew.add("int");
                                //System.out.println("int");
                            } else {
                                
                                columnsDataTypesNew.add("String");
                                //System.out.println("string");
                            }
                             counterOfcolumnsNames++;
                            
                        } 
                        else if (counterOfcolumnsNames != columnsNames.size()
                                && !columnsNames.get(counterOfcolumnsNames).equals(columnsNamesInXML.get(index))){
                            columnsDataTypesNew.add("null");
                            //System.out.println("null");
                        }
                       
                }
                    /*
                    System.out.println();
                    for (int countOfCheck = 0; countOfCheck < columnsDataTypesNew.size(); countOfCheck++){
                        System.out.println(columnsDataTypesNew.get(countOfCheck));
                    }
                    */
                    for (int countOfCheckDTD = 0; countOfCheckDTD < columnsDataTypesNew.size(); countOfCheckDTD++){
                        if (!columnsDataTypesNew.get(countOfCheckDTD).equals(columnsDataTypesFromDTD.get(countOfCheckDTD))
                                && !columnsDataTypesNew.get(countOfCheckDTD).equals("null")){
                            JOptionPane.showMessageDialog(null,
                                "Incompitable data types..", "Display message ", JOptionPane.INFORMATION_MESSAGE);
                            return 0;
                        }
                    }
                }   
                
                /*
                    for (int index = 0; index < columnsNamesInXML.size(); index++){
                        if (dummyN.getNodeType() == Node.ELEMENT_NODE
                                && counterOfcolumnsNames != columnsNames.size()
                                && !dummyN.getNodeName().equals(columnsNamesInXML.indexOf(index))) {
                            columnsContentsDataTypes.add("null");
                            //counterOfcolumnsNames++;
                        } 
                    }*/
                
                /*
                Node nod;
                int indexOfColUser = 0;
                for (int indexOfColNames = 0; indexOfColNames < list.getLength(); indexOfColNames++){
                    node = list.item(indexOfColNames);
                    if (node.getNodeType() == Node.ELEMENT_NODE){
                        if (indexOfColUser != columnsNames.size() 
                                && node.getNodeName().equals(columnsNames.get(indexOfColUser))){
                            
                            indexOfColUser++;
                        }
                    }
                }*/

//                ArrayList<String> colDTs = new ArrayList<String>();
//                for (int i = 0; i < list.getLength(); i++){
//                    Node n = list.item(i);
//                    if (n.getNodeName().equals(columnsNames.get(i))){
//                        colDTs.add(columnsContentsDataTypes.get(i));
//                        System.out.println(colDTs.get(i));
//                    }
//                }
                /*
                Node dummy2;
                
                for (int i = 0; i < columnsDTOfDTDRistricted.size(); i++){
                    dummy2 = list.item(i);

                    if (dummy2.getNodeType() == Node.ELEMENT_NODE) {
                        if (!columnsContentsDataTypes.get(i).equals(columnsDTOfDTDRistricted.get(i))){
                            JOptionPane.showMessageDialog(null,
                                    "Incompitable data types.\nTry again.", "Error Message ", JOptionPane.ERROR_MESSAGE);
                            //System.out.println("ay 7aga");
                            return 0;
                        }
                    }
                }
                */

    /*
                for (int i = 0; i < columnsDTOfDTDRistricted.size(); i++) {
                    //node = list.item(i);

                    //if (node.getNodeType() == Node.ELEMENT_NODE) {
                        if (!columnsContentsDataTypes.get(i).equals(columnsDTOfDTDRistricted.get(i))) {
                            JOptionPane.showMessageDialog(null,
                                    "Incompitable data types.\nTry again.", "Error Message ", JOptionPane.ERROR_MESSAGE);
                            //System.out.println("kldjfsld");
                            return 0;
                        }

                    //}
                }
*/  /*
                int iOfColNames = 0;
                for (int counterOfNodes = 0; counterOfNodes < list.getLength(); counterOfNodes++){
                    Node dummyN = list.item(counterOfNodes);
                    if (dummyN.getNodeType() == Node.ELEMENT_NODE
                            && dummyN.getNodeName().equals(columnsNames.get(iOfColNames))){
                        if (!columnsDataTypesFromDTD.get(iOfColNames).equals(columnsDTOfDTDRistricted.get(iOfColNames))){
                           
                            System.out.println("error");
                            iOfColNames++;
                        }
                        
                    }
                    
                }
                */
                if (flag == 0) {
                    for (int i = 0; i < list.getLength(); i++) {
                        node = list.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            if (j != columnsNames.size() && columnsNames.get(j).equals(node.getNodeName())) {
                                node.setTextContent(columnsContents.get(j));
                                j++;
                            }
                        }
                    }

                } else if (flag == 1) {
                    //System.out.println("flag = 1");

                    //doc = new Document();
                    // Creates an element named table and makes it the root
                    Element theRoot = new Element(tableName);
                    Node table2 = table.cloneNode(true);

                    //Element theRoot = new Element("row");
                    //doc.createElement(theRoot.getName());
                    //Node tableForRow2 = table;
                    j = 0;
                    NodeList listForRow2 = table2.getChildNodes();
                    for (int i = 0; i < listForRow2.getLength(); i++) {
                        node = listForRow2.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            if (j != columnsNames.size() && columnsNames.get(j).equals(node.getNodeName())) {
                                node.setTextContent(columnsContents.get(j));
                                j++;
                            }
                            else{
                                node.setTextContent("\u200B");
                            }
                        }
                    }
                    ay7aga.appendChild(table2);

                }
                // write the content into xml file
                DOMSource source = new DOMSource((org.w3c.dom.Node) doc);
                StreamResult result = new StreamResult(new File("xml/" + fileName));
                transformer.transform(source, result);
                }        
             catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (TransformerException tfe) {
                tfe.printStackTrace();
            }
        
        JOptionPane.showMessageDialog(null,
                "Insertion is complete.", "Display message ", JOptionPane.INFORMATION_MESSAGE);
//        for (int removeIndex = 0; removeIndex < columnsContents.size(); removeIndex++){
//            columnsContents.remove(removeIndex);
//        }
        return counterOfColumns;
        }
    }

    public int deleteFromTable(String tableName, String condition) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, JDOMException, TransformerConfigurationException, TransformerException {

        int counterOfColumns = 0, counterOfSpaces = 0;

        String fileName = tableName + ".xml", filePath = "xml/" + fileName, operator = null, x = "", value = "";

        String[] arrayOfCondition = new String[3];

        for (int dummyCounter = 0; dummyCounter < tableName.length(); dummyCounter++) {
            if (tableName.charAt(dummyCounter) == ' ') {
                counterOfSpaces++;
            }
        }
        
        int flag = 0;
        
        if (counterOfSpaces == 0 || counterOfSpaces == 1) {
            arrayOfCondition = validateConditionWithoutSpace(condition);
        } else if (counterOfSpaces == 2) {
            if (validateCondition(condition) == null) {
                return 0;
            } else {
                arrayOfCondition = validateCondition(condition);
            }
        } else {
            JOptionPane.showMessageDialog(null,
                    "The condition is written in a wrong way.\n"
                            + "Check the # of spaces", "Error Message ", JOptionPane.ERROR_MESSAGE);
        }

        x = arrayOfCondition[0];
        operator = arrayOfCondition[1];
        value = arrayOfCondition[2];

        if (operator.charAt(0) == '>' || operator.charAt(0) == '<') {
            for (int counterOfValue = 0; counterOfValue < value.length(); counterOfValue++) {
                if (!Character.isDigit(value.charAt(counterOfValue))) {
                    JOptionPane.showMessageDialog(null,
                            "You can't find the greatest of 2 strings", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    return 0;
                }
            }
        }

        File file = new File(filePath);

        ArrayList<String> columnsNames = new ArrayList<String>();
        //columnsNames.add("null");

        if (!file.exists()) {
            //System.out.println("asmaa");
            JOptionPane.showMessageDialog(null,
                    "File doesn't exist", "Error Message ", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
        
        else {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();

            org.w3c.dom.Document doc = docBuilder.newDocument();

            // Solving the problem of -> writing the tableName besides the xml version    
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // Convert from org.w3c.Document into JDOM Document
            DOMBuilder in = new DOMBuilder();
            Document jdomDoc = in.build(doc);

            doc = docBuilder.parse(new File(filePath));

            NodeList list;

            List<Node> delete = new ArrayList<Node>();
            
            //list = doc.getElementsByTagName("row");
            //int length = list.getLength();
            //XPath xPath = XPathFactory.newInstance().newXPath();
            //XPathExpression xExpress = xPath.compile("/"+tableName);
            //SAXBuilder builder = new SAXBuilder();
            //Document readDoc = builder.build(new File(filePath));
            //BufferedReader br = null;
            //FileReader fr = null;
            //fr = new FileReader(filePath);
            //br = new BufferedReader(fr);
            //String sCurrentLine = br.readLine();
            //sCurrentLine = br.readLine();
            //br = new BufferedReader(new FileReader(filePath));
            //if (operator.charAt(0) == '=') {
            //do {
            list = doc.getElementsByTagName("row");
            
            
//            Node node2 = listClone.item(0);
//            System.out.println(node2.getNodeName());
//            NodeList childList2 = node2.getChildNodes();
//            int length = childList2.getLength();
//            System.out.println(childList2.getLength());
//            for (int simpleCount = 0; simpleCount < childList2.getLength(); simpleCount++){
//                    
//                    Node no = childList2.item(simpleCount);
//                    //NodeList child 
//                    if (no.getNodeType() == Node.ELEMENT_NODE){
//                    System.out.println(no.getNodeName());
//                    }
//                    
//            
//                }

            
            //ArrayList <String> columnsNames = new ArrayList<String>();
            //length --;
            //System.out.println("Number of Rows" + list.getLength());
            counterOfColumns = list.getLength();
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                //System.out.println("================ Row #" + i);
//                if(i < 8 )
//                    continue;
                NodeList childList = node.getChildNodes();
               
                //System.out.println(node.getNodeName());
                //columnsNames.add(childList.item(i));
                
                //int justCounter = 0;
                for (int k = 0; k < childList.getLength(); k++) {
                    Node child = childList.item(k);
//                    if (child.getNodeType() == Node.ELEMENT_NODE){
//                        justCounter++;
//                        System.out.println(justCounter);
//                    }
                    
                    //System.out.println(justCounter);
                    // To search only "row" children
                    if (operator.charAt(0) == '=') {
                        if (child.getNodeType() == Node.ELEMENT_NODE) {
                            //System.out.println("> " + i + ": " + k);
                            if (child.getNodeName().equalsIgnoreCase(x)
                                    && child.getTextContent().equalsIgnoreCase(value)){
                                //counterOfColumns++;
                                //columnsNames.add(child.getNodeName());
                                //if (child.getNodeName().equalsIgnoreCase(x) && child.getTextContent().equalsIgnoreCase(value)) {

                                //System.out.println(child.getTextContent());
                                //if (!child.getTextContent().equalsIgnoreCase("\u200B")){
                                delete.add(node);
                                counterOfColumns--;
                                flag = 1;
                              //  System.out.println(k + " is a match");
                             //   System.out.println("BREAK");
                                break;
                                //}
                                //}
                            } else {
                             //   System.out.println(k + " text not equal taregt value");
                            }
                        } else {
                           // System.out.println(k + " not an element node");
                        }
                    } else if (operator.charAt(0) == '<'){
                        if (child.getNodeType() == Node.ELEMENT_NODE) {
                            if (child.getNodeName().equalsIgnoreCase(x)) {
                                //int flag = 0;
                                //for (int counterOfWord = 0; counterOfWord < child.getTextContent().length(); counterOfWord++) {
                                //System.out.println(child.getTextContent());
                                String s = child.getTextContent();
                                int intOfString;
                                if (child.getTextContent().charAt(0) == ' ') {
                                    StringBuilder sb = new StringBuilder(s);
                                    sb.deleteCharAt(0);
                                    String finalResult = sb.toString();
                                    intOfString = Integer.parseInt(finalResult);
                                    //System.out.println(intOfString);
                                    //System.out.println(Integer.parseInt(value));
                                }
                                else{
                                    intOfString = Integer.parseInt(s);
                                }
                                int val = Integer.parseInt(value);
                                if (intOfString < val) {
                                    //if (!child.getTextContent().equalsIgnoreCase("\u200B")){
                                    delete.add(node);
                                    counterOfColumns--;
                                    flag = 1;
                                    //System.out.println(counterOfColumns);
                                    //}
                                    break;
                                }
                            }
                        }
                    } else if (operator.charAt(0) == '>') {
                        if (child.getNodeType() == Node.ELEMENT_NODE) {
                            if (child.getNodeName().equalsIgnoreCase(x)) {
                                String s = child.getTextContent();
                                int intOfString;
                                if (child.getTextContent().charAt(0) == ' ') {
                                    StringBuilder sb = new StringBuilder(s);
                                    sb.deleteCharAt(0);
                                    String finalResult = sb.toString();
                                    intOfString = Integer.parseInt(finalResult);
                                    //System.out.println(intOfString);
                                    //System.out.println(Integer.parseInt(value));
                                }
                                else{
                                    intOfString = Integer.parseInt(s);
                                }
                                int valueNum = Integer.parseInt(value);
                                //  for (int counterOfWord = 0; counterOfWord < child.getTextContent().length(); counterOfWord++) {
                                if (intOfString > valueNum) {
                                    //System.out.println(value);
                                    //System.out.println(child.getTextContent());
                                    //if (!child.getTextContent().equalsIgnoreCase("\u200B")){
                                    delete.add(node);
                                    counterOfColumns--;
                                    flag = 1;
                                    //System.out.println(counterOfColumns);
                                    break;
                                    //}
                                }
                            }
                        }
                    }
                }
                
                /*
                Node saveColNames;
                if (!delete.isEmpty()){
                    saveColNames = delete.get(0);
                }
                else {
                    return 0;
                }
                */
                
                
                /*
                NodeList nodeList;
                Node node_delete;
                
                
                for (int m = 0; m < delete.size(); m++) {
                    //Node nodeToDelete = delete.get(m);
                    //nodeToDelete.getParentNode().removeChild(nodeToDelete);
                    nodeList = delete.get(m);
                    for (int l = 0; l < nodeList.getLength(); l++){
                        node_delete = nodeList.item(l);
                        node_delete.getParentNode().removeChild(node_delete);
                        
                    }
                    
                }
                */
                
                /*
                        Transformer tf = TransformerFactory.newInstance().newTransformer();
                        tf.setOutputProperty(OutputKeys.INDENT, "yes");
                        tf.setOutputProperty(OutputKeys.METHOD, "xml");
                        tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                        DOMSource domSource = new DOMSource(doc);
                        StreamResult sr = new StreamResult(System.out);
                        tf.transform(domSource, sr);
                        System.out.println("successful delete:)");
                 */
             //   if (flag == 0) {
             //       JOptionPane.showMessageDialog(null,
             //               "Condition doesn't match any record", "Error Message ", JOptionPane.ERROR_MESSAGE);
              //      return -1;
              //  }
            
            }

            for (int counter = 0; counter < delete.size(); counter++) {
                Node nodeToDelete = delete.get(counter);
                nodeToDelete.getParentNode().removeChild(nodeToDelete);
            }
            for (int counterOfDelNodes = 0; counterOfDelNodes < delete.size(); counterOfDelNodes++) {
                delete.remove(counterOfDelNodes);
            }
            /*
            int justCounterOfColumns = 0;
            Node dummyNode = list.item(0);
            NodeList dummyList = dummyNode.getChildNodes();
            for (int dummyCounter = 0; dummyCounter < dummyList.getLength(); dummyCounter++){
                Node n = dummyList.item(dummyCounter);
                if (n.getNodeType() == Node.ELEMENT_NODE){
                    justCounterOfColumns++;
                    System.out.println(justCounterOfColumns);
                }
            }
            */
            
            try {
                DOMParser parser = new DOMParser();
                parser.parse(filePath);
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(filePath));
                transformer.transform(source, result);
            } catch (IOException io) {
                io.printStackTrace();
            }
            
           
            // if (counterOfColumns != 1){
            /*
            if (counterOfColumns == 1) {

                file.delete();

                String fileName_toCreate = tableName + ".xml";

                File idea = new File("xml/" + fileName_toCreate);

                FileWriter fw = new FileWriter(idea);

                BufferedWriter bw = new BufferedWriter(fw);

                Document document = new Document();

                Element theRoot = new Element(tableName);
                Element show = new Element("row");
                document.setRootElement(theRoot);

                Element col;

                int i = 0;

                for (String c : columnsNames) {
                    if (c != "null") {
                        col = new Element(c);

                        col.addContent(new Text("\u200B"));

                        show.addContent(col);

                        i++;
                    }
                }
                theRoot.addContent(show);
                XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());

                xmlOutput.output(document, fw);
*/
                
         //   }
            //}
            Scanner fileScan;
                PrintWriter writer;

                try {

                    File originalFile = new File(filePath);
                    fileScan = new Scanner(originalFile);

                    writer = new PrintWriter("xml/ay7aga.xml");

                    while (fileScan.hasNext()) {
                        String line = fileScan.nextLine();
                        //System.out.println(line);
                        if (!line.isEmpty() && !line.equals("  ")) {
                            writer.write(line);
                            //System.out.println(line);
                            writer.write("\n");
                        }
                    }

                    fileScan.close();
                    writer.close();
                    originalFile.delete();
                    File f = new File("xml/ay7aga.xml");
                    f.renameTo(originalFile);

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Action.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                /*
                org.w3c.dom.Document docNew;
                
                
                
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilderNew = docFactory.newDocumentBuilder();
                
                
		docNew = docBuilderNew.parse(filePath);

                DOMBuilder inNew = new DOMBuilder();
                Document jdomDoc2 = inNew.build(docNew);
                
		// Get the root element
		if (docNew.getFirstChild() == null){
                    
                }
                */
                
        //}
            //while (length != 0);
                
            
            

//                try {
//                
//                File originalFile2 = new File(filePath);
//                
//                
//                Scanner fileScan2 = new Scanner(originalFile2);
//
//                PrintWriter writer2 = new PrintWriter("xml/ay7aga.xml");
//
//                while (fileScan2.hasNext()) {
//                    String line = fileScan2.nextLine();
//                    //System.out.println(line);
//                    if (!line.isEmpty() || !line.equals("  ")) {
//                        writer2.write(line);
//                        //System.out.println(line);
//                        writer2.write("\n");
//                    }
//                }
//
//                fileScan2.close();
//                writer2.close();
//                originalFile2.delete();
//                File f = new File("xml/ay7aga.xml");
//                f.renameTo(originalFile2);
//
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(Action.class.getName()).log(Level.SEVERE, null, ex);
//            }
        
            Scanner fileScan2;
            File checkFile = new File(filePath);
            fileScan2 = new Scanner(checkFile);

            String lineToRead = fileScan2.nextLine();
            //System.out.println(line);
            lineToRead = fileScan2.nextLine();
            //System.out.println(line);
            lineToRead = fileScan2.nextLine();
            //System.out.println(lineToRead);
            if (!fileScan2.hasNext()){
                checkFile.delete();
                File dtdFile = new File("xml/"+tableName+".dtd");
                dtdFile.delete();
                return 1;
            }
    }
        
        
            
        
        
        
        //line = fileScan2.nextLine();
        //System.out.println(line);
        
        //counterOfColumns = 0;
        //System.out.println(counterOfColumns);
        
        
        return counterOfColumns;
    }

    public Object[][] selectFromTable(String tableName, String condition, List<String> columnsNames) throws FileNotFoundException, ParserConfigurationException, TransformerConfigurationException, SAXException, IOException, TransformerException, XPathExpressionException {

        //Object object[][];
        ArrayList<String> selectedItems = new ArrayList<String>();

        int counterOfObject = 0, counterOfSpaces = 0;

        String fileName = tableName + ".xml", filePath = "xml/" + fileName, operator = null, x = "", value = "";

        String[] arrayOfCondition = new String[3];

        //arrayOfCondition = validateCondition(condition);
        for (int dummyCounter = 0; dummyCounter < tableName.length(); dummyCounter++) {
            if (tableName.charAt(dummyCounter) == ' ') {
                counterOfSpaces++;
            }
        }

        if (counterOfSpaces == 0) {
            arrayOfCondition = validateConditionWithoutSpace(condition);
        } else {
            arrayOfCondition = validateCondition(condition);

        }

        x = arrayOfCondition[0];
        operator = arrayOfCondition[1];
        value = arrayOfCondition[2];

        if (operator.charAt(0) != '=') {
            for (int counterOfValue = 0; counterOfValue < value.length(); counterOfValue++) {
                if (value.charAt(counterOfValue) < '0' || value.charAt(counterOfValue) > '9') {
                    JOptionPane.showMessageDialog(null,
                    "You can't find the greatest of 2 strings", "Error Message ", JOptionPane.ERROR_MESSAGE);
                    return null;
                }
            }
        }

        File file = new File(filePath);
        //if (file.exists()) {
        /*
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();

            org.w3c.dom.Document doc = docBuilder.newDocument();

            // Solving the problem of -> writing the tableName besides the xml version    
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // Convert from org.w3c.Document into JDOM Document
            DOMBuilder in = new DOMBuilder();
            Document jdomDoc = in.build(doc);

            doc = docBuilder.parse(new File(filePath));
         */

        FileInputStream fileToSelectFrom = new FileInputStream(new File(filePath));

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        org.w3c.dom.Document xmlDocument = builder.parse(fileToSelectFrom);

        XPath xPath = XPathFactory.newInstance().newXPath();

        //System.out.println("*************************");
        String expression = "/" + tableName + "/row/" + x;
        //System.out.println(expression);

        ArrayList<String> arrayOfColumnsNamesExp = new ArrayList<String>();
        ArrayList<String> correspondingColumnsNames = new ArrayList<String>();

        for (int counterOfColumnsNames = 0; counterOfColumnsNames < columnsNames.size(); counterOfColumnsNames++) {
            if (!columnsNames.get(counterOfColumnsNames).equalsIgnoreCase(x)) {
                arrayOfColumnsNamesExp.add("/" + tableName + "/row/" + columnsNames.get(counterOfColumnsNames));
                correspondingColumnsNames.add(columnsNames.get(counterOfColumnsNames));
                //System.out.println(arrayOfColumnsNamesExp);
                //System.out.println(columnsNames.get(counterOfColumnsNames));
                //System.out.println(counterOfColumnsNames);
            }

        }
        int counterOfRows = 0;

        NodeList nodeList1 = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

        for (int i = 0; i < nodeList1.getLength(); i++) {
            //System.out.println(nodeList.item(i).getNodeName());
            //System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
            if (nodeList1.item(i).getNodeType() == Node.ELEMENT_NODE) {
                if (nodeList1.item(i).getTextContent().equalsIgnoreCase(value)) {
                    counterOfRows++;
                    //System.out.println(counterOfRows);
                }
            }
        }
        int counterOfRowsForGreater = 0, counterOfRowsForSmaller = 0;
        if (operator.charAt(0) == '<'){
        for (int i = 0; i < nodeList1.getLength(); i++){
            if (nodeList1.item(i).getNodeType() == Node.ELEMENT_NODE) {
                if (Integer.parseInt(nodeList1.item(i).getTextContent()) < Integer.parseInt(value)) {
                    counterOfRowsForSmaller++;
                    //System.out.println(counterOfRowsForSmaller);
                }
            }
        }}
        
        if (operator.charAt(0) == '>'){
            for (int i = 0; i < nodeList1.getLength(); i++){
                if (nodeList1.item(i).getNodeType()== Node.ELEMENT_NODE){
                    if (Integer.parseInt(nodeList1.item(i).getTextContent()) > Integer.parseInt(value)){
                        counterOfRowsForGreater++;
                    }
                }
            }
        }
        
        //System.out.println(counterOfRows);
        String string, string2 = null;
        Object[][] object = null;
        
        if (operator.charAt(0) == '='){
            object = new Object[counterOfRows][columnsNames.size()];
        }
        else if (operator.charAt(0) == '<'){
            object = new Object[counterOfRowsForSmaller][columnsNames.size()];
        }
        else if (operator.charAt(0) == '>'){
            object = new Object[counterOfRowsForGreater][columnsNames.size()];
        }
        
        
        
        
        int newCounter = 0, indexOfRow = 0, flagOfRows = 0, flagSmaller = 0, flagGreater = 0, newCounterForGreater = 0,
                newCounterForSmaller = 0;
        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {
            //System.out.println(nodeList.item(i).getNodeName());
            //System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
            
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE
                    && nodeList.item(i).getNodeName().equalsIgnoreCase(x)) {
                //System.out.println("dfdsfsdf");
                
                if (operator.charAt(0) == '='
                        && nodeList.item(i).getFirstChild().getNodeValue().equalsIgnoreCase(value)) {
                    
                    for (int counterOfNames = 0; counterOfNames < columnsNames.size(); counterOfNames++) {
//                        if (!columnsNames.get(counterOfNames).equalsIgnoreCase(nodeList.item(i).getNodeName())){
//                            string2 = nodeList.item(i).getTextContent();
//                        }
                        if (columnsNames.get(counterOfNames).equalsIgnoreCase(nodeList.item(i).getNodeName())) {
                            //string = nodeList.item(i).getTextContent();
                            //System.out.println(string);
                            //System.out.println(nodeList.item(i).getNodeName());
                            //System.out.println(nodeList.item(i).getNodeName() + " = " + nodeList.item(i).getFirstChild().getNodeValue());
                            //
                            selectedItems.add(nodeList.item(i).getNodeName() + " = " + nodeList.item(i).getFirstChild().getNodeValue());
                            //System.out.println(newCounter);
                            
                            //System.out.println(newCounter);
                            //System.out.println(counterOfObject);
                            if (counterOfObject != columnsNames.size() 
                                    && newCounter != counterOfRows){
                            object[newCounter][counterOfObject] = nodeList.item(i).getFirstChild().getNodeValue();
                            //System.out.println(newCounter);
                            //System.out.println(counterOfObject);
                            //System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
                            //System.out.println(object[newCounter][counterOfObject]);
                            //if (object[newCounter][counterOfObject] != null){
                            //System.out.println(counterOfObject);
                            counterOfObject++;
                            flagOfRows = 1;
                            }
                            
                            //System.out.println(newCounter);
                                //System.out.println(counterOfObject);
                                //System.out.println(counterOfObject);
                            //}
                            //counterOfRows++;
                            //
//                            System.out.println(string);
//                            System.out.println(string2);
//                            if (string.equalsIgnoreCase(value) && !string.equals(string2)){
//                                newCounter++;
//                            //    System.out.println(newCounter);
//                            }
//                            
//                            System.out.println(newCounter);
//                            System.out.println(counterOfObject);
                           
                        }
                    }
                    

                    for (int j = 0; j < arrayOfColumnsNamesExp.size(); j++) {
                        XPath xpath = XPathFactory.newInstance().newXPath();
                        XPathExpression xpr = xpath.compile(arrayOfColumnsNamesExp.get(j));

                        NodeList subNodeList = (NodeList) xpr.evaluate(xmlDocument, XPathConstants.NODESET);

                        if (subNodeList.item(j).getFirstChild().getNodeValue().equals("\u200B")) {
                            //System.out.println(correspondingColumnsNames.get(j) + " is empty");
                            if (counterOfObject != columnsNames.size() 
                                    && newCounter != counterOfRows){
                            object[newCounter][counterOfObject]
                                    = correspondingColumnsNames.get(j) + " is empty";
                            counterOfObject++;
                            }
                            //System.out.println(object[newCounter][counterOfObject]);
                            //System.out.println(counterOfObject);
                            
                            //System.out.println(newCounter);
                            selectedItems.add(correspondingColumnsNames.get(j) + " is empty");

                        } else {
                            //System.out.println(subNodeList.item(i).getFirstChild().getNodeValue());
                            //System.out.println(newCounter);
                            //System.out.println(counterOfObject);
                            if (counterOfObject != columnsNames.size() 
                                    && newCounter != counterOfRows){
                            object[newCounter][counterOfObject]
                                    = subNodeList.item(i).getFirstChild().getNodeValue();
                            counterOfObject++;
                            }
                            //System.err.println(object[newCounter][counterOfObject]);
                            //System.out.println(subNodeList.item(i).getFirstChild().getNodeValue());
                            //System.out.println(counterOfObject);
                            //counterOfObject++;
                            //System.out.println(newCounter);
                            selectedItems.add(correspondingColumnsNames.get(j) + " = " + subNodeList.item(i).getFirstChild().getNodeValue());
                        }

                    }
                    //System.out.println();
                } else if (operator.charAt(0) == '<'
                        && Integer.parseInt(nodeList.item(i).getFirstChild().getNodeValue())
                        < Integer.parseInt(value)) {
                        flagSmaller = 1;
                        
                    for (int counterOfNames = 0; counterOfNames < columnsNames.size(); counterOfNames++) {
                        if (columnsNames.get(counterOfNames).equalsIgnoreCase(nodeList.item(i).getNodeName())) {
                        //    System.out.println(nodeList.item(i).getFirstChild().getNodeValue());
                            //
                            if (counterOfObject != columnsNames.size() 
                                    && newCounterForSmaller != counterOfRows){
                            selectedItems.add(nodeList.item(i).getFirstChild().getNodeValue());
                               // System.out.println(newCounter);
                            object[newCounterForSmaller][counterOfObject]
                                    = nodeList.item(i).getFirstChild().getNodeValue();
                            //System.out.println(object[newCounterForSmaller][counterOfObject] + "1");
//                            counterOfObject++;
//                            counterOfRows++;
                            counterOfObject++;
                            //flag1= 1;
                             //   System.out.println();
                            //flagOfRows = 1;
                            //flagOfSmaller = 1;
                            //flagOfRows = 1;
                            
                            //
                            }
                        }
                    }

                    for (int j = 0; j < arrayOfColumnsNamesExp.size(); j++) {
                        XPath xpath = XPathFactory.newInstance().newXPath();
                        XPathExpression xpr = xpath.compile(arrayOfColumnsNamesExp.get(j));

                        NodeList subNodeList = (NodeList) xpr.evaluate(xmlDocument, XPathConstants.NODESET);
                        
                        
                        if (subNodeList.item(j).getFirstChild().getNodeValue().equals("\u200B")) {
                            if (counterOfObject != columnsNames.size()
                                    && newCounterForSmaller != counterOfRows) {
                                
                                object[newCounterForSmaller][counterOfObject]
                                        = correspondingColumnsNames.get(j) + " is empty";
                                //System.out.println(object[newCounterForSmaller][counterOfObject]+"2");
                                counterOfObject++;
                                
                                selectedItems.add(correspondingColumnsNames.get(j) + " is empty");
                                //System.out.println(correspondingColumnsNames.get(j) + " is empty");
                            }
                        } else {
                            if (counterOfObject != columnsNames.size()
                                && newCounterForSmaller != counterOfRows) {
                            //System.out.println(subNodeList.item(i).getFirstChild().getNodeValue());
                            object[newCounterForSmaller][counterOfObject]
                                    = subNodeList.item(i).getFirstChild().getNodeValue();
                            //System.out.println(object[newCounterForSmaller][counterOfObject]+"3");
                            counterOfObject++;
                            }
                        }
                    }
                    //System.out.println();
                } else if (operator.charAt(0) == '>'
                        && Integer.parseInt(nodeList.item(i).getFirstChild().getNodeValue())
                        > Integer.parseInt(value)) {
                    flagGreater = 1;
                    for (int counterOfNames = 0; counterOfNames < columnsNames.size(); counterOfNames++) {
                        if (columnsNames.get(counterOfNames).equalsIgnoreCase(nodeList.item(i).getNodeName())) {
                            //System.out.println(nodeList.item(i).getNodeName() + " = " + nodeList.item(i).getFirstChild().getNodeValue());
                            //
                            if (counterOfObject != columnsNames.size() 
                                    && newCounter != counterOfRowsForGreater){
                            selectedItems.add(nodeList.item(i).getNodeName() + " = " + nodeList.item(i).getFirstChild().getNodeValue());
                            object[newCounter][counterOfObject]
                                    = nodeList.item(i).getFirstChild().getNodeValue();
                                
                            counterOfObject++;
                            flagOfRows = 1;
                            
//
                            }
                        }
                    }

                    for (int j = 0; j < arrayOfColumnsNamesExp.size(); j++) {
                        XPath xpath = XPathFactory.newInstance().newXPath();
                        XPathExpression xpr = xpath.compile(arrayOfColumnsNamesExp.get(j));

                        NodeList subNodeList = (NodeList) xpr.evaluate(xmlDocument, XPathConstants.NODESET);

                        if (subNodeList.item(j).getFirstChild().getNodeValue().equals("\u200B")) {
                            if (counterOfObject != columnsNames.size() 
                                    && newCounter != counterOfRowsForGreater){
                            //System.out.println(correspondingColumnsNames.get(j) + " is empty");
                            object[newCounter][counterOfObject]
                                    = correspondingColumnsNames.get(j) + " is empty";
                            counterOfObject++;
                        }}
                        else {
                            if (counterOfObject != columnsNames.size() 
                                    && newCounter != counterOfRowsForGreater){
                            //System.out.println(correspondingColumnsNames.get(j) + " = " + subNodeList.item(i).getFirstChild().getNodeValue());
                            object[newCounter][counterOfObject]
                                    = subNodeList.item(i).getFirstChild().getNodeValue();
                            counterOfObject++;
                        }}

                    }
                    //System.out.println();
                }
                

                 if (operator.charAt(0) == '=' && flagOfRows == 1){
                   newCounter++;      
                }
                 else if (operator.charAt(0) == '<' && flagSmaller == 1) {
                     newCounterForSmaller++;
                 }
                 else if (operator.charAt(0) == '>' && flagGreater == 1){
                     
                 }
                   counterOfObject = 0;
                   //System.out.println(counterOfRows);
                   flagOfRows = 0;
            }
               
        }
        // }
        /*
        for (int count1 = 0; count1 < counterOfRowsForSmaller; count1++){
            for (int count2 = 0; count2 < columnsNames.size(); count2++){
                System.out.println(object[count1][count2]);
            }
        }
        */
        return object;
    }

    /*
            NodeList list;

            List<Node> select = new ArrayList<Node>();

            list = doc.getElementsByTagName("row");

            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                NodeList childList = node.getChildNodes();

                for (int k = 0; k < childList.getLength(); k++) {

                    Node child = childList.item(k);

                    // To search only "row" children
                    if (operator.charAt(0) == '=') {
                        if (child.getNodeType() == Node.ELEMENT_NODE
                                && child.getNodeName().equalsIgnoreCase(x)
                                && child.getTextContent().equalsIgnoreCase(value)) {
                            //System.out.println(child.getTextContent());
                            select.add(node);
                            break;
                        }
                    } else if (operator.charAt(0) == '<'
                            && child.getNodeType() == Node.ELEMENT_NODE
                            && child.getNodeName().equalsIgnoreCase(x)) {
                        //int flag = 0;
                        //for (int counterOfWord = 0; counterOfWord < child.getTextContent().length(); counterOfWord++) {

                        if (Integer.parseInt(child.getTextContent()) < Integer.parseInt(value)) {
                            select.add(node);
                            break;
                        }
                    } else if (operator.charAt(0) == '>'
                            && child.getNodeType() == Node.ELEMENT_NODE
                            && child.getNodeName().equalsIgnoreCase(x)) {
                        int childNum = Integer.parseInt(child.getTextContent());
                        int valueNum = Integer.parseInt(value);
                        //  for (int counterOfWord = 0; counterOfWord < child.getTextContent().length(); counterOfWord++) {
                        if (childNum > valueNum) {
                            //System.out.println(value);
                            //System.out.println(child.getTextContent());
                            select.add(node);
                            break;
                        }
                    }
                }
     */
 /*
                for (int m = 0; m < select.size(); m++) {
                    Node nodeToDelete = select.get(m);
                    nodeToDelete.getParentNode().removeChild(nodeToDelete);
                }
     */
 /*
                for (int counterOfDelNodes = 0; counterOfDelNodes < select.size(); counterOfDelNodes++) {
                    Node nodeToDisplay = select.get(counterOfDelNodes);
          //          System.out.println(nodeToDisplay.getFirstChild().getNodeName()+nodeToDisplay.getTextContent());
                   
                }
     */
 /*
                    if (delete.size() == 0)
                    {
                        System.out.println("ajdlakjsdklasjd");
                    }
     */
 /*
                        Transformer tf = TransformerFactory.newInstance().newTransformer();
                        tf.setOutputProperty(OutputKeys.INDENT, "yes");
                        tf.setOutputProperty(OutputKeys.METHOD, "xml");
                        tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                        DOMSource domSource = new DOMSource(doc);
                        StreamResult sr = new StreamResult(System.out);
                        tf.transform(domSource, sr);
                 //       System.out.println("successful delete:)");
     */ /*      
            }

            try {
                DOMParser parser = new DOMParser();
                parser.parse(filePath);
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(filePath));
                transformer.transform(source, result);
            } catch (IOException io) {
                io.printStackTrace();

            }*/

    private String[] validateCondition(String condition) {

        String[] arrayOfCondition = new String[3];

        int counterOfSpaces = 0;

        for (int i = 0; i < condition.length(); i++) {
            if (Character.isSpaceChar(condition.charAt(i))) {
                counterOfSpaces++;
            }
            /*
            if (Character.isSpaceChar(condition.charAt(i)) && Character.isSpaceChar(condition.charAt(i + 1))) {
                System.out.println("The condition isn't valid\nHint: use spaces in your condition");
                System.exit(1);
            }
             */
        }

        if (condition.length() < 5) {
            JOptionPane.showMessageDialog(null,
                    "The condition isn't valid.", "Error Message ", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        if (counterOfSpaces == 2) {
            Scanner in = new Scanner(new String(condition));
            String tokens[] = condition.split(" ");
            arrayOfCondition[0] = tokens[0];
            arrayOfCondition[1] = tokens[1];
            arrayOfCondition[2] = tokens[2];
        }

        if (arrayOfCondition[1].length() != 1 || Character.getType(arrayOfCondition[1].charAt(0)) != Character.MATH_SYMBOL) {
            System.out.println("The condition isn't valid\nHint: use right spaces in your condition");
            System.exit(1);
        }
        return arrayOfCondition;
    }

    private String[] validateConditionWithoutSpace(String condition) {
        String x = "", value = "";
        char dummyOperator = '0';
        String operator = "";

        String[] arrayOfString = new String[3];

        int counterOfSpace = 0, indexOfSpace = 0;
        String s = condition;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                counterOfSpace = 1;
                break;
            }
        }

        if (counterOfSpace == 1) {
            StringBuilder sb = new StringBuilder(s);
            sb.deleteCharAt(indexOfSpace);
            condition = sb.toString();
        }

        if (condition.length() < 3) {
            JOptionPane.showMessageDialog(null,
                    "The condition isn't valid.", "Error Message ", JOptionPane.ERROR_MESSAGE);
        }

        for (int i = 0; i < condition.length(); i++) {
            dummyOperator = condition.charAt(i);

            if (dummyOperator == '=') {
                operator = "=";
                break;
            } else if (dummyOperator == '>') {
                operator = ">";
                break;
            } else if (dummyOperator == '<') {
                operator = "<";
                break;
            }
            x += dummyOperator;
        }
        // Suggestion: change "System.exit(1)" into "return 0" later
        if (dummyOperator == condition.charAt(condition.length() - 1)
                || condition.charAt(0) == '=' || condition.charAt(0) == '>' || condition.charAt(0) == '<') {
            System.out.println("The condition isn't valid");
            System.exit(1);
        }

        for (int i = x.length() + 1; i < condition.length(); i++) {
            value += condition.charAt(i);
        }

        arrayOfString[0] = x;
        arrayOfString[1] = operator;
        arrayOfString[2] = value;

        return arrayOfString;
    }

};

//{     
//  System.out.println("Deleted");
//}
/*
                else if (copyFile.exists())
                {

                    fw = new FileWriter(copyFile.getAbsoluteFile(), true);
                    bw = new BufferedWriter(fw);
                    
                    while (scanFile.hasNext()) {
                        String line = scanFile.nextLine();
                        if (!line.isEmpty()) {

                            fw.write(line);
                            fw.write("\n");
                        }
                    }

                    scanFile.close();
                    fw.close();
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Action.class.getName()).log(Level.SEVERE, null, ex);
            }
 */
//}
//    } while (currentNodeList.equals('\0'));
/*   //  }
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression xExpress = xPath.compile("/" + tableName);
            NodeList nodeList = (NodeList) xExpress.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
 */
// XXX collection of nodes to delete XXX
/*
        for (int i = 0; i < list.getLength(); i++) {

            Node node = list.item(i);
            NodeList childList = node.getChildNodes();

            // Looking through all children nodes
            for (int x = 0; x < childList.getLength(); x++) {

                Node child = childList.item(x);

                // To search only "book" children
                if (child.getNodeType() == Node.ELEMENT_NODE
                        && child.getNodeName().equalsIgnoreCase("name")
                        && child.getTextContent().toUpperCase().equalsIgnoreCase("abcd".toUpperCase())) {
                    // XXX just add to "to be deleted" list XXX
                    delete.add(node);
                    break;
                }
            }

        }

// XXX delete nodes XXX
        for (int i = 0; i < delete.size(); i++) {
            Node node = delete.get(i);
            node.getParentNode().removeChild(node);
        }

 */
//        
//        
//        try {
//            
//            // Parses the file supplied into a JDOM document
//SAXBuilder builder = new SAXBuilder();
//        
//        try {
//            
//            // Parses the file supplied into a JDOM document
//
//            doc = builder.build("./src/jdomMade.xml");
//            
//            System.out.println("Root: " + doc.getRootElement());
//            Node node;
//            
//            //DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//            //doc = (Document) builder.parse("./src/jdomMade.xml");
//
//        }
//
//        catch (JDOMException e) {
//            e.printStackTrace();
//
//        }
//
//        catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//            doc = builder.build("./src/jdomMade.xml");
//            
//            System.out.println("Root: " + doc.getRootElement());
//            Node node;
//            
//            //DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//            //doc = (Document) builder.parse("./src/jdomMade.xml");
//
//        }
//
//        catch (JDOMException e) {
//            e.printStackTrace();
//
//        }
//
//        catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

/*
            File xmlFile = new File("/data/test/test.xml");
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = (Document) dBuilder.parse(xmlFile);

    NodeList eventList = doc.getElementsByTagName("DEvent");
    for (int i=0; i< eventList.getLength(); i++) {
        Node thisNode = (Node) eventList.item(i);

        if(thisNode.hasChildNodes()){
            NodeList event = thisNode.getChildNodes().getFirstChild().getNextSibling();
            String eid = event.getAttributes().getNamedItem("eid");
            String value = event.getFirstChild().getNodeValue();
            System.out.println("EID: " + eid + " Value: " + value + "\n");
        }
 */
 /*
        String fileName = tableName+".xml";
                
        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = tableName+"/row/";
        InputSource inputSource = new InputSource(new BufferedReader(new FileReader("xml/"+fileName)));
        
        NodeList eventList = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);

        for (int i = 0; i < eventList.getLength(); i++) {
            Node eventNode = (Node) eventList.item(i);
            
            String eid = xpath.evaluate("@eid", eventNode);
            String value = xpath.evaluate("./text()", eventNode);
            System.out.println("EID: " + eid + ", Value: " + value + "\n");
        }
 */
